﻿using System.IO;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using TC.EMEA.PropertyEtl.Common.Handlers;
using TC.EMEA.PropertyEtl.Common.Models;

namespace TC.EMEA.PropertyEtl.Business.Handlers
{
    public class PropertyBlobStreamHandler : IHandler<Stream, S2Property>
    {
        private readonly ILogger _logger;

        public PropertyBlobStreamHandler(ILogger logger)
        {
            _logger = logger;
        }

        public S2Property Execute(Stream request)
        {
            try
            {
                var reader = new StreamReader(request);
                reader.BaseStream.Seek(0, SeekOrigin.Begin);
                var rawMessage = reader.ReadToEnd();
                JsonSerializerSettings serializerSettings = new JsonSerializerSettings() // ignore errors, 
                {
                    Error = (sender, args) =>
                    {
                        args.ErrorContext.Handled = true;
                    }
                };
                var property = JsonConvert.DeserializeObject<S2Property>(rawMessage, serializerSettings);
                return property;
            }
            catch (System.Exception exception)
            {
                _logger.LogCritical($"Failed to parse {nameof(S2Property)} from request, Exception: {exception.ToString()}");
                throw;
            }
        }

       
    }
}