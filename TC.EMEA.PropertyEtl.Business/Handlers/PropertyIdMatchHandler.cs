﻿using System.Linq;
using AutoMapper;
using Microsoft.Extensions.Logging;
using TC.EMEA.PropertyEtl.Common;
using TC.EMEA.PropertyEtl.Common.Data;
using TC.EMEA.PropertyEtl.Common.Handlers;
using TC.EMEA.PropertyEtl.Common.Models;
using TC.EMEA.PropertyEtl.Common.Specifications;

namespace TC.EMEA.PropertyEtl.Business.Handlers
{
    public class PropertyIdMatchHandler : BaseHandler, IPropertyIdMatchHandler
    {
        public PropertyIdMatchHandler(IRepository<Property> propertyRepository, IMapper mapper) : base(propertyRepository, mapper)
        {
        }
        protected override Property Find(Property input)
        {
            if (string.IsNullOrEmpty(input.SourcePropertyId) || string.Equals(input.SourcePropertyId, "0")) return null;
            return this.PropertyRepository.Find(new PropertyIdMatchSpecification(input)).FirstOrDefault();
        }

        protected override Property Map(Property source, Property destination)
        {
            return this.Mapper.Map(source, destination, opts => opts.Items[Constants.Mapping.OVERWRITE_SOURCE_INFO] = false);
        }
    }
};