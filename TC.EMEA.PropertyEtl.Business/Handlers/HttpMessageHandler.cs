﻿using Microsoft.AspNetCore.Http;
using TC.EMEA.PropertyEtl.Common.Handlers;
using TC.EMEA.PropertyEtl.Common.Models;

namespace TC.EMEA.PropertyEtl.Business.Handlers
{
    public class HttpMessageHandler : IHandler<HttpRequest, Property>
    {
        private readonly IHandler<S1Property, Property> _s1PropertyConverter;
        private readonly IHandler<HttpRequest, S1Property> _requestConverter;

        public HttpMessageHandler(IHandler<S1Property, Property> s1PropertyConverter, IHandler<HttpRequest,S1Property> requestConverter)
        {
            _s1PropertyConverter = s1PropertyConverter;
            _requestConverter = requestConverter;
        }
        public Property Execute(HttpRequest request)
        {
            var s1Property = _requestConverter.Execute(request);
            var property = _s1PropertyConverter.Execute(s1Property);
            return property;
        }
    }
}