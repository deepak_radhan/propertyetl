﻿using AutoMapper;
using Microsoft.Extensions.Logging;
using TC.EMEA.PropertyEtl.Common;
using TC.EMEA.PropertyEtl.Common.Data;
using TC.EMEA.PropertyEtl.Common.Handlers;
using TC.EMEA.PropertyEtl.Common.Models;
using TC.EMEA.PropertyEtl.Common.Responses;

namespace TC.EMEA.PropertyEtl.Business.Handlers
{
    public class NewPropertyHandler : INewPropertyHandler
    {
        private readonly IRepository<Property> _propertyRepository;
        private readonly IMapper _mapper;
        public NewPropertyHandler(IRepository<Property> propertyRepository, IMapper mapper)
        {
            _propertyRepository = propertyRepository;
            _mapper = mapper;
        }

        public PropertyOrchestrationResponse Execute(Property input)
        {      
            if(input == null) return PropertyOrchestrationResponse.FailureResponse;
            if(string.Equals(input.SourcePropertyId, "0")) return PropertyOrchestrationResponse.FailureResponse;
            var mappedNewProperty = _mapper.Map<Property>(input, opts => opts.Items[Constants.Mapping.OVERWRITE_SOURCE_INFO] = false);
            this._propertyRepository.Insert(mappedNewProperty);
            return PropertyOrchestrationResponse.SuccessResponse;
        }
    }
}