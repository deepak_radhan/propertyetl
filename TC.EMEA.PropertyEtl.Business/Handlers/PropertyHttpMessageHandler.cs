﻿using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using TC.EMEA.PropertyEtl.Common.Handlers;
using TC.EMEA.PropertyEtl.Common.Models;
using ErrorEventArgs = Newtonsoft.Json.Serialization.ErrorEventArgs;

namespace TC.EMEA.PropertyEtl.Business.Handlers
{
    public class PropertyHttpMessageHandler<TProperty> : IHandler<HttpRequest, TProperty> where TProperty : BaseEntity
    {
        private readonly ILogger _logger;

        public PropertyHttpMessageHandler(ILogger logger)
        {
            _logger = logger;
        }
        public TProperty Execute(HttpRequest input)
        {
            try
            {
                var reader = new StreamReader(input.Body);
                reader.BaseStream.Seek(0, SeekOrigin.Begin);
                var rawMessage = reader.ReadToEnd();
                JsonSerializerSettings serializerSettings = new JsonSerializerSettings()
                {
                    Error = (sender, args) =>
                    {
                        args.ErrorContext.Handled = true;
                    }
                };
                var property = JsonConvert.DeserializeObject<TProperty>(rawMessage, serializerSettings);
                return property;
            }
            catch (System.Exception exception)
            {
                _logger.LogCritical($"Failed to parse [{typeof(TProperty).Name}] from request, Exception: {exception.ToString()}");
                throw;
            }
        }

    }
}