﻿using System;
using System.Globalization;
using System.Net.NetworkInformation;
using System.Text.RegularExpressions;
using Microsoft.Extensions.Primitives;
using TC.EMEA.PropertyEtl.Common;
using TC.EMEA.PropertyEtl.Common.Enums;
using TC.EMEA.PropertyEtl.Common.Handlers;
using TC.EMEA.PropertyEtl.Common.Models;

namespace TC.EMEA.PropertyEtl.Business.Handlers
{
    public class S1PropertyHandler : IHandler<S1Property, Property>
    {
        public Property Execute(S1Property input)
        {
            var property = new Property();
            var tempProperty = new Property();
            tempProperty.SourceSystem = SourceSystem.SourceSystem1;
            tempProperty.SourcePropertyId = input.Id.HasValue ? input.Id.ToString() : null; // TODO : Verify in test case
            if (!string.IsNullOrEmpty(input.Address1))
            {
                var splitValues = SplitAddress1(input.Address1);
                tempProperty.Name = splitValues.name.Trim();
                tempProperty.Address1 = splitValues.line1.Trim();
            }

            if (!string.IsNullOrEmpty(input.Address2))
            {
                var splitValues = SplitAddress2(input.Address2);
                tempProperty.PostCode = splitValues.postCode.Trim();
                if (!string.IsNullOrEmpty(splitValues.city))
                {
                    tempProperty.City = splitValues.city.Trim();
                }

                if( string.Equals(splitValues.city, string.Empty) && (string.Equals(splitValues.postCode,string.Empty)) )
                {
                    tempProperty.Address2 = splitValues.address2.Trim();
                    tempProperty.City = string.Empty;
                }
            }

            tempProperty.Latitude = input.Lat;
            tempProperty.Longitude = input.Lon;
            tempProperty.FloorCount = input.FloorCount;

            property = tempProperty;
            return property;
        }

        private static (string name, string line1) SplitAddress1(string addressLine1)
        {
            if (addressLine1.IndexOf(',') == -1)
            {
                return (string.Empty, addressLine1);
            }
            (string name, string line1) tempTuple = (string.Empty, string.Empty);
            var splitValues = addressLine1.Split(',', StringSplitOptions.RemoveEmptyEntries);
            if (splitValues.Length >= 2)
            {
                tempTuple.name = splitValues[0];
                tempTuple.line1 = splitValues[1];
            }
            return tempTuple;
        }

        private static (string city, string postCode, string address2) SplitAddress2(string addressLine2)
        {
            (string city, string postCode, string address2) tempTuple = (string.Empty, string.Empty, string.Empty);
            var match = Regex.Match(addressLine2, Constants.Regex.UK_POST_CODE_REGEX);
            if (match.Success)
            {
                tempTuple.postCode = match.Value;
                var city = addressLine2.Replace(match.Value, string.Empty);
                tempTuple.city = city;
                tempTuple.address2 = string.Empty;
            }
            else
            {
                tempTuple.city = string.Empty;
                tempTuple.postCode = string.Empty;
                tempTuple.address2 = addressLine2;
            }
            return tempTuple;
        }
        
    }
}