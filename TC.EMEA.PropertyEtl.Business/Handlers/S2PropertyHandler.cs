﻿using System;
using System.Globalization;
using System.Text.RegularExpressions;
using TC.EMEA.PropertyEtl.Common;
using TC.EMEA.PropertyEtl.Common.Enums;
using TC.EMEA.PropertyEtl.Common.Handlers;
using TC.EMEA.PropertyEtl.Common.Models;

namespace TC.EMEA.PropertyEtl.Business.Handlers
{
    public class S2PropertyHandler : IHandler<S2Property, Property>
    {
        public Property Execute(S2Property s2Property)
        {
            var property = new Property();
            var tempProperty = new Property();
            tempProperty.SourceSystem = SourceSystem.SourceSystem2;
            tempProperty.SourcePropertyId = s2Property.Id.HasValue ? s2Property.Id.ToString() : null;
            tempProperty.Name = s2Property.Name;

            if (!string.IsNullOrEmpty(s2Property.Address))
            {
                var addressSplit = SplitAddress(s2Property.Address);
                tempProperty.City = addressSplit.city;
                tempProperty.Address1 = addressSplit.line1;
            }

            tempProperty.PostCode = s2Property.PostCode;
            if (s2Property.Coordinates != null && s2Property.Coordinates.Length >= 2)
            {
                tempProperty.Latitude = s2Property.Coordinates[0];
                tempProperty.Longitude = s2Property.Coordinates[1];
            }

            tempProperty.FloorArea = s2Property.FloorArea;

            property = tempProperty;
            return property;
        }

        private static (string line1, string city) SplitAddress(string address)
        {
            (string line1, string city) tempTuple = (string.Empty, string.Empty);
            var splitValues = address.Split(',', StringSplitOptions.RemoveEmptyEntries);

            if (splitValues.Length >= 2)
            {
                tempTuple.line1 = splitValues[0];
                if (!string.IsNullOrEmpty(splitValues[1]))
                {
                    var match = Regex.Match(splitValues[1], Constants.Regex.UK_POST_CODE_REGEX);
                    var city = splitValues[1].Replace(match.Value, string.Empty);
                    if (!string.IsNullOrEmpty(city))
                    {
                        tempTuple.city = city.Trim();
                    }
                }
            }

            return tempTuple;
        }

       
    }
}