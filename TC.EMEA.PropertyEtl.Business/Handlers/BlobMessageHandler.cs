﻿using System.IO;
using TC.EMEA.PropertyEtl.Common.Handlers;
using TC.EMEA.PropertyEtl.Common.Models;

namespace TC.EMEA.PropertyEtl.Business.Handlers
{
    public class BlobMessageHandler : IHandler<Stream,Property>
    {
        private readonly IHandler<S2Property, Property> _s2PropertyConverter;
        private readonly IHandler<Stream, S2Property> _requestConverter;

        public BlobMessageHandler(IHandler<S2Property, Property> s2PropertyConverter, IHandler<Stream, S2Property> requestConverter)
        {
            _s2PropertyConverter = s2PropertyConverter;
            _requestConverter = requestConverter;
        }

        public Property Execute(Stream request)
        {
            var s2Property = _requestConverter.Execute(request);
            var property = _s2PropertyConverter.Execute(s2Property);
            return property;
        }
    }
}