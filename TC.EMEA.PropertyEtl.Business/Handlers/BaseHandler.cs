﻿using AutoMapper;
using Microsoft.Extensions.Logging;
using TC.EMEA.PropertyEtl.Business.Orchestrators;
using TC.EMEA.PropertyEtl.Common.Data;
using TC.EMEA.PropertyEtl.Common.Handlers;
using TC.EMEA.PropertyEtl.Common.Models;
using TC.EMEA.PropertyEtl.Common.Responses;

namespace TC.EMEA.PropertyEtl.Business.Handlers
{
    public abstract class BaseHandler : IHandler<Property, PropertyOrchestrationResponse>
    {
        protected readonly IRepository<Property> PropertyRepository;
        protected readonly IMapper Mapper;
        


        protected BaseHandler(IRepository<Property> propertyRepository, IMapper mapper)
        {
            PropertyRepository = propertyRepository;
            Mapper = mapper;
        }

        public PropertyOrchestrationResponse Execute(Property incomingProperty)
        {
            var matchedProperty = this.Find(incomingProperty);
            if (matchedProperty != null)
            {
                var mappedProperty = this.Map(incomingProperty,matchedProperty);
                this.PropertyRepository.Update(mappedProperty);
                return PropertyOrchestrationResponse.SuccessResponse;
            }
            return PropertyOrchestrationResponse.FailureResponse;
        }
        protected abstract Property Find(Property input);
        protected abstract Property Map(Property source, Property destination);
    }
}