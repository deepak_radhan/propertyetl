﻿using System.Linq;
using AutoMapper;
using Microsoft.Extensions.Logging;
using TC.EMEA.PropertyEtl.Business.Orchestrators;
using TC.EMEA.PropertyEtl.Common;
using TC.EMEA.PropertyEtl.Common.Data;
using TC.EMEA.PropertyEtl.Common.Handlers;
using TC.EMEA.PropertyEtl.Common.Models;
using TC.EMEA.PropertyEtl.Common.Specifications;

namespace TC.EMEA.PropertyEtl.Business.Handlers
{
    public class PropertyPostCodeMatchHandler : BaseHandler, IPropertyPostCodeMatchHandler
    {      
        public PropertyPostCodeMatchHandler(IRepository<Property> propertyRepository, IMapper mapper) : base(propertyRepository,mapper)
        {        
        }

        protected override Property Find(Property input)
        {
            if (string.IsNullOrEmpty(input.PostCode)) return null;
            Property property = null;
            var matchingProperties = this.PropertyRepository.Find(new PropertyPostCodeMatchSpecification(input));
            if (matchingProperties != null)
            {
                property = matchingProperties.FirstOrDefault();
            }
            return property;
        }
            
        protected override Property Map(Property source, Property destination)
        {
            return this.Mapper.Map<Property, Property>(source, destination, opts => opts.Items[Constants.Mapping.OVERWRITE_SOURCE_INFO] = true);
        }
    }
}