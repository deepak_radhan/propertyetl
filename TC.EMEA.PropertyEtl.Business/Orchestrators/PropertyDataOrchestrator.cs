﻿using System;
using AutoMapper;
using Microsoft.Extensions.Logging;
using Microsoft.VisualBasic;
using TC.EMEA.PropertyEtl.Business.Handlers;
using TC.EMEA.PropertyEtl.Common.Data;
using TC.EMEA.PropertyEtl.Common.Handlers;
using TC.EMEA.PropertyEtl.Common.Models;
using TC.EMEA.PropertyEtl.Common.Orchestrators;
using TC.EMEA.PropertyEtl.Common.Requests;
using TC.EMEA.PropertyEtl.Common.Responses;

namespace TC.EMEA.PropertyEtl.Business.Orchestrators
{
    public class PropertyDataOrchestrator : IOrchestrator<PropertyOrchestrationRequest, PropertyOrchestrationResponse>
    {
        private readonly IPropertyIdMatchHandler _idMatchHandler;
        private readonly IPropertyNameMatchHandler _nameMatchHandler;
        private readonly IPropertyPostCodeMatchHandler _postCodeMatchHandler;
        private readonly INewPropertyHandler _newPropertyHandler;


        public PropertyDataOrchestrator(IPropertyIdMatchHandler idMatchHandler, IPropertyNameMatchHandler nameMatchHandler,
                                        IPropertyPostCodeMatchHandler postCodeMatchHandler, INewPropertyHandler newPropertyHandler)
        {
            _idMatchHandler = idMatchHandler;
            _nameMatchHandler = nameMatchHandler;
            _postCodeMatchHandler = postCodeMatchHandler;
            _newPropertyHandler = newPropertyHandler;
        }

        public PropertyOrchestrationResponse Execute(PropertyOrchestrationRequest request)
        {
            if (request == null) throw new ArgumentNullException(nameof(request));
            var response = new ChainHandler()
                .AddHandler(_idMatchHandler)
                .AddHandler(_nameMatchHandler)
                .AddHandler(_postCodeMatchHandler)
                .AddHandler(_newPropertyHandler)
                .Handle(request);
            return response;
        }      
    }



}