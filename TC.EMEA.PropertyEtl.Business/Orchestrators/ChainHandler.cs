﻿using System.Collections.Generic;
using TC.EMEA.PropertyEtl.Common.Handlers;
using TC.EMEA.PropertyEtl.Common.Models;
using TC.EMEA.PropertyEtl.Common.Requests;
using TC.EMEA.PropertyEtl.Common.Responses;

namespace TC.EMEA.PropertyEtl.Business.Orchestrators
{
    public class ChainHandler
    {
        private List<IHandler<Property, PropertyOrchestrationResponse>> _list = new List<IHandler<Property, PropertyOrchestrationResponse>>();
        public ChainHandler AddHandler(IHandler<Property, PropertyOrchestrationResponse> newHandler)
        {
            _list.Add(newHandler);
            return this;
        }
        PropertyOrchestrationResponse response = null;
        public PropertyOrchestrationResponse Handle(PropertyOrchestrationRequest request)
        {
            for (int i = 0; i < _list.Count; i++)
            {
                response = _list[i].Execute(request.Property);
                if (response.IsSuccess)
                    break;
            }
            return response;
        }

    }
}