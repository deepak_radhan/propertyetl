﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Extensions.Logging;
using System.Web.Http;
using TC.EMEA.PropertyEtl.Common;
using TC.EMEA.PropertyEtl.Common.Handlers;
using TC.EMEA.PropertyEtl.Common.Models;
using TC.EMEA.PropertyEtl.Common.Orchestrators;
using TC.EMEA.PropertyEtl.Common.Requests;
using TC.EMEA.PropertyEtl.Common.Responses;

namespace TC.EMEA.PropertyEtl.Functions
{
    public class HttpTriggers
    {
        private readonly IHandler<HttpRequest, Property> _httpRequestHandler;
        private readonly IOrchestrator<PropertyOrchestrationRequest, PropertyOrchestrationResponse> _propertyDataOrchestrator;

        public HttpTriggers(IHandler<HttpRequest, Property> httpRequestHandler, IOrchestrator<PropertyOrchestrationRequest, PropertyOrchestrationResponse> propertyDataOrchestrator)
        {
            _httpRequestHandler = httpRequestHandler;
            _propertyDataOrchestrator = propertyDataOrchestrator;
        }

        [FunctionName("HttpGateway")]
        public ActionResult Get([HttpTrigger(AuthorizationLevel.Function, "POST")] HttpRequest request, ILogger logger)
        {
            logger.LogInformation("C# HTTP trigger function processed a request.");
            var property =  _httpRequestHandler.Execute(request);
            var isValid = Validator.ValidateMessage(property);
            if(!isValid) return new BadRequestObjectResult("Invalid Property info");
            PropertyOrchestrationRequest orchestrationRequest = new PropertyOrchestrationRequest()
            {
                Property = property
            };
            var orchestrationResult = _propertyDataOrchestrator.Execute(orchestrationRequest);
            
            if(orchestrationResult.IsSuccess)
                return new OkResult();
            else
                return new BadRequestObjectResult("Invalid property information - Id, Name and PostCode missing");
        }
    }
}
