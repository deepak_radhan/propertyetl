using System.IO;
using Microsoft.Azure.WebJobs;
using Microsoft.Extensions.Logging;
using TC.EMEA.PropertyEtl.Common;
using TC.EMEA.PropertyEtl.Common.Handlers;
using TC.EMEA.PropertyEtl.Common.Models;
using TC.EMEA.PropertyEtl.Common.Orchestrators;
using TC.EMEA.PropertyEtl.Common.Requests;
using TC.EMEA.PropertyEtl.Common.Responses;

namespace TC.EMEA.PropertyEtl.Functions
{
    public class BlobTriggers
    {
        private readonly IHandler<Stream, Property> _streamRequestHandler;
        private readonly IOrchestrator<PropertyOrchestrationRequest, PropertyOrchestrationResponse> _propertyDataOrchestrator;

        public BlobTriggers(IHandler<Stream, Property> streamRequestHandler, IOrchestrator<PropertyOrchestrationRequest, PropertyOrchestrationResponse> propertyDataOrchestrator)
        {
            _streamRequestHandler = streamRequestHandler;
            _propertyDataOrchestrator = propertyDataOrchestrator;
        }

        [FunctionName("BlobTriggers")]
        public void Run([BlobTrigger("s2drop/{name}", Connection = "BlobConnection")]
            Stream myBlob, string name, ILogger logger)
        {
            logger.LogInformation($"S2Property trigger function Processed blob\n Name:{name} \n Size: {myBlob.Length} Bytes");
            var property = _streamRequestHandler.Execute(myBlob);
            var isValid = Validator.ValidateMessage(property);
            if (!isValid)
            {
                logger.LogInformation($"Invalid Property info in file");
                return;
            }
            PropertyOrchestrationRequest orchestrationRequest = new PropertyOrchestrationRequest()
            {
                Property = property
            };

            var orchestrationResult = _propertyDataOrchestrator.Execute(orchestrationRequest);
            var outcome = orchestrationResult.IsSuccess ? "Success" : "Failure";
            logger.LogInformation($"File processing completed with {outcome}");
            
        }
    }
}
