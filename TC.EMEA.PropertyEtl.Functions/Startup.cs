﻿using System;
using System.IO;
using System.Reflection.Metadata.Ecma335;
using System.Threading.Channels;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.Azure.Functions.Extensions.DependencyInjection;
using Microsoft.Azure.WebJobs.Host.Bindings;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using TC.EMEA.PropertyEtl.Business.Handlers;
using TC.EMEA.PropertyEtl.Business.Orchestrators;
using TC.EMEA.PropertyEtl.Common;
using TC.EMEA.PropertyEtl.Common.Data;
using TC.EMEA.PropertyEtl.Common.Extensions;
using TC.EMEA.PropertyEtl.Common.Handlers;
using TC.EMEA.PropertyEtl.Common.Mapping.Profiles;
using TC.EMEA.PropertyEtl.Common.Models;
using TC.EMEA.PropertyEtl.Common.Orchestrators;
using TC.EMEA.PropertyEtl.Common.Requests;
using TC.EMEA.PropertyEtl.Common.Responses;
using TC.EMEA.PropertyEtl.Data.Contexts;
using TC.EMEA.PropertyEtl.Data.Repositories;

[assembly: FunctionsStartup(typeof(TC.EMEA.PropertyEtl.Functions.Startup))]
namespace TC.EMEA.PropertyEtl.Functions
{
    public class Startup : FunctionsStartup
    {
        public override void Configure(IFunctionsHostBuilder builder)
        {
            var executionContextOptions = builder.Services.BuildServiceProvider().GetService<IOptions<ExecutionContextOptions>>();
            var appDirectory = executionContextOptions.Value.AppDirectory;

            var environment = Environment.GetEnvironmentVariable("Environment");
            var configFileName = $"config.{environment}.json";

            var config = new ConfigurationBuilder().SetBasePath(appDirectory)
                .AddJsonFile(configFileName)
                .AddEnvironmentVariables()
                .Build();

            var middlewareSettings = config.GetConfigSection<CosmosSettings>("CosmosSettings");

            builder.Services.AddLogging();
            builder.Services.AddTransient<IHandler<HttpRequest, Property>, TC.EMEA.PropertyEtl.Business.Handlers.HttpMessageHandler>();
            builder.Services.AddTransient<IHandler<S1Property, Property>, S1PropertyHandler>();
            builder.Services.AddTransient<IHandler<HttpRequest, S1Property>, PropertyHttpMessageHandler<S1Property>>();
            builder.Services.AddTransient<IOrchestrator<PropertyOrchestrationRequest, PropertyOrchestrationResponse>,PropertyDataOrchestrator>();

            builder.Services.AddTransient<IHandler<Stream, Property>, BlobMessageHandler>();
            builder.Services.AddTransient<IHandler<S2Property, Property>, S2PropertyHandler>();
            builder.Services.AddTransient<IHandler<Stream, S2Property>, PropertyBlobStreamHandler>();

            builder.Services.AddTransient<INewPropertyHandler, NewPropertyHandler>();
            builder.Services.AddTransient<IPropertyNameMatchHandler, PropertyNameMatchHandler>();
            builder.Services.AddTransient<IPropertyPostCodeMatchHandler, PropertyPostCodeMatchHandler>();
            builder.Services.AddTransient<IPropertyIdMatchHandler, PropertyIdMatchHandler>();

            builder.Services.AddDbContext<DbContext, PropertiesContext>(options => options
                            .UseCosmos(middlewareSettings.Endpoint, middlewareSettings.AuthKey, middlewareSettings.DataBaseName));
            builder.Services.AddTransient(typeof(IRepository<>), typeof(Repository<>));

            builder.Services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());

            var mappingConfig = new MapperConfiguration(cfg => {
                cfg.AddProfile<PropertyProfile>();
            });
            var mapper = mappingConfig.CreateMapper();
            builder.Services.AddSingleton(mapper);
        }

        private string PrintFoo() => "First";

        private void DoNothing()
        {
            return;
        }

        private void NewMethodInMasterForRebase()
        {
            return;
        }

        private void NewMethodInBranchForRebase()
        {
            return;
        }
    }
}