﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TC.EMEA.PropertyEtl.Functions
{
    public class Person
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string Line1 { get; set; }
        public string Line2 { get; set; }
        public string PostCode { get; set; }
        public override string ToString()
        {
            return $"{Id} - {Name}";
        }
        public  string PersonString()
        {
            return $"{Id} - {Name} - {Address}";
        }
        public  bool PersonEquals(object? obj)
        {
            return base.Equals(obj);
        }
        public override bool Equals(object? obj)
        {
            return base.Equals(obj);
        }

        public void Audit()
        {
            return;
        }

        public void HotFixAudit()
        {
            return;
        }

        public void HotFixAudit1()
        {
            return;
        }
    }
}