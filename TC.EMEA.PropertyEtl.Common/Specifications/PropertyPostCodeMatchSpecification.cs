﻿using System;
using System.Linq.Expressions;
using TC.EMEA.PropertyEtl.Common.Models;

namespace TC.EMEA.PropertyEtl.Common.Specifications
{
    public class PropertyPostCodeMatchSpecification : AbstractSpecification<Property>
    {
        private readonly Property _property;

        public PropertyPostCodeMatchSpecification(Property property)
        {
            _property = property;
        }

        public override Expression<Func<Property, bool>> ToExpression()
        {
            return x => string.Equals(x.PostCode, _property.PostCode, StringComparison.InvariantCultureIgnoreCase);
        }
    }
}