﻿using System;
using System.Linq.Expressions;

namespace TC.EMEA.PropertyEtl.Common.Specifications
{
    public abstract class AbstractSpecification<TEntity>
    {
        public abstract Expression<Func<TEntity, bool>> ToExpression();

        public bool IsSatisfiedBy(TEntity entity)
        {
            Func<TEntity, bool> predicate = ToExpression().Compile();
            return predicate(entity);
        }
    }
}