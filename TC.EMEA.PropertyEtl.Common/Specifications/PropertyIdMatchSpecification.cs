﻿using System;
using System.Linq.Expressions;
using TC.EMEA.PropertyEtl.Common.Models;

namespace TC.EMEA.PropertyEtl.Common.Specifications
{
    public class PropertyIdMatchSpecification : AbstractSpecification<Property>
    {
        private readonly Property _property;

        public PropertyIdMatchSpecification(Property property)
        {
            _property = property;
        }

        public override Expression<Func<Property, bool>> ToExpression()
        {
            return x => x.SourceSystem == _property.SourceSystem && string.Equals(x.SourcePropertyId,_property.SourcePropertyId, StringComparison.InvariantCultureIgnoreCase);
        }
    }
}