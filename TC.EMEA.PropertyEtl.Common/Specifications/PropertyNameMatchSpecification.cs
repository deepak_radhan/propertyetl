﻿using System;
using System.Linq.Expressions;
using TC.EMEA.PropertyEtl.Common.Models;

namespace TC.EMEA.PropertyEtl.Common.Specifications
{
    public class PropertyNameMatchSpecification : AbstractSpecification<Property>
    {
        private readonly Property _property;

        public PropertyNameMatchSpecification(Property property)
        {
            _property = property;
        }

        public override Expression<Func<Property, bool>> ToExpression()
        {
            return x => string.Equals(x.Name, _property.Name, StringComparison.InvariantCultureIgnoreCase);
        }
    }
}