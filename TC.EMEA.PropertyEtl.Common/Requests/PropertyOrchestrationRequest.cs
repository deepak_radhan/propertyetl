﻿using TC.EMEA.PropertyEtl.Common.Models;

namespace TC.EMEA.PropertyEtl.Common.Requests
{
    public class PropertyOrchestrationRequest : OrchestrationRequest
    {
        public Property Property { get; set; }
    }
}