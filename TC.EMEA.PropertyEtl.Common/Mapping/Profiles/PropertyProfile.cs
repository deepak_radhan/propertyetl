﻿using AutoMapper;
using TC.EMEA.PropertyEtl.Common.Mapping.Converters;
using TC.EMEA.PropertyEtl.Common.Models;

namespace TC.EMEA.PropertyEtl.Common.Mapping.Profiles
{
    public class PropertyProfile : Profile
    {
        public PropertyProfile()
        {
            CreateMap<Property, Property>().ConvertUsing<PropertyConverter>();
        }
    }

    
}