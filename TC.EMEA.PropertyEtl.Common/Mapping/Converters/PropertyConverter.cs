﻿using AutoMapper;
using System;
using System.Security.Cryptography;
using TC.EMEA.PropertyEtl.Common.Models;

namespace TC.EMEA.PropertyEtl.Common.Mapping.Converters
{
    public class PropertyConverter : ITypeConverter<Property, Property>
    {
        public Property Convert(Property source, Property destination, ResolutionContext context)
        {
            if(destination == null)
            {
                destination = new Property();
                destination.PropertyId = Guid.NewGuid();
                destination.SourcePropertyId = source.SourcePropertyId;
                destination.SourceSystem = source.SourceSystem;
            }
            if (!string.IsNullOrEmpty(source.Name)) destination.Name = source.Name;
            if (!string.IsNullOrEmpty(source.Address1)) destination.Address1 = source.Address1;
            if (!string.IsNullOrEmpty(source.Address2)) destination.Address2 = source.Address2;
            if (!string.IsNullOrEmpty(source.City)) destination.City = source.City;
            if (!string.IsNullOrEmpty(source.PostCode)) destination.PostCode = source.PostCode;
            if (source.Latitude.HasValue) destination.Latitude = source.Latitude;
            if (source.Longitude.HasValue) destination.Longitude = source.Longitude;
            if (source.FloorArea.HasValue) destination.FloorArea = source.FloorArea;
            if (source.FloorCount.HasValue)destination.FloorCount = source.FloorCount;

            if (context.Items.TryGetValue(Constants.Mapping.OVERWRITE_SOURCE_INFO, out object isSourceInfoOverWrite))
            {
               if(isSourceInfoOverWrite is bool value && value)
               {
                   if(!string.IsNullOrEmpty(source.SourcePropertyId)) destination.SourcePropertyId = source.SourcePropertyId;
                   destination.SourceSystem = source.SourceSystem;
               }
            }
            return destination;
        }
    }
}