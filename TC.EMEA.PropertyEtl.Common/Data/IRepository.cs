﻿using System;
using System.Collections.Generic;
using TC.EMEA.PropertyEtl.Common.Specifications;

namespace TC.EMEA.PropertyEtl.Common.Data
{
    public interface IRepository<TEntity>
    {
        IEnumerable<TEntity> Get(Func<TEntity, bool> predicate);
        void Insert(TEntity entity);
        void Update(TEntity entity);
        IEnumerable<TEntity> Find(AbstractSpecification<TEntity> specification);
    }
}