﻿using System;
using Microsoft.Extensions.Configuration;

namespace TC.EMEA.PropertyEtl.Common.Extensions
{
    public static class ConfigurationBindingExtensions
    {
        public static T GetConfigSection<T>(this IConfiguration configuration, string name = null)
        {
            var instance = Activator.CreateInstance<T>();
            if (string.IsNullOrWhiteSpace(name))
            {
                configuration.Bind(instance);
                return instance;
            }
            configuration.Bind(name, instance);
            return instance;
        }
    }
}