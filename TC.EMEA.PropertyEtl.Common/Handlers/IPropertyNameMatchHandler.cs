﻿using TC.EMEA.PropertyEtl.Common.Models;
using TC.EMEA.PropertyEtl.Common.Responses;

namespace TC.EMEA.PropertyEtl.Common.Handlers
{
    public interface IPropertyNameMatchHandler : IHandler<Property, PropertyOrchestrationResponse>
    {
    }
}