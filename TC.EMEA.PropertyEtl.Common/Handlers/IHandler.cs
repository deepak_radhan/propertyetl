﻿namespace TC.EMEA.PropertyEtl.Common.Handlers
{
    public interface IHandler<in TInput, out TOutput>
    {
        TOutput Execute(TInput request);
    }
}