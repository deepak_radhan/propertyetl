﻿namespace TC.EMEA.PropertyEtl.Common.Responses
{
    public class PropertyOrchestrationResponse : OrchestrationResponse
    {
        private  PropertyOrchestrationResponse()
        {
        }

        public static PropertyOrchestrationResponse SuccessResponse = new PropertyOrchestrationResponse() { IsSuccess = true};
        public static PropertyOrchestrationResponse FailureResponse = new PropertyOrchestrationResponse() { IsSuccess = false};
        public bool IsSuccess { get; set; }
    }
}