﻿namespace TC.EMEA.PropertyEtl.Common.Enums
{
    public enum SourceSystem
    {
        Unknown = 0,
        SourceSystem1 = 1,
        SourceSystem2 = 2
    }
}