﻿using TC.EMEA.PropertyEtl.Common.Models;

namespace TC.EMEA.PropertyEtl.Common
{
    public static class Validator
    {
        public static bool ValidateMessage(Property property)
        {
            return (property != null &&
                    (!(string.IsNullOrEmpty(property.SourcePropertyId) &&
                       string.IsNullOrEmpty(property.Name) &&
                       string.IsNullOrEmpty(property.PostCode)
                       ))
                );
        }
    }
}