﻿namespace TC.EMEA.PropertyEtl.Common
{
    public static class Constants
    {
        public static class Mapping
        {
            public static string OVERWRITE_SOURCE_INFO = "overWriteSourceInfo";
        }

        public static class Regex
        {
            public static string UK_POST_CODE_REGEX =
                @"([Gg][Ii][Rr] 0[Aa]{2})|((([A-Za-z][0-9]{1,2})|(([A-Za-z][A-Ha-hJ-Yj-y][0-9]{1,2})|(([A-Za-z][0-9][A-Za-z])|([A-Za-z][A-Ha-hJ-Yj-y][0-9][A-Za-z]?))))\s?[0-9][A-Za-z]{2})";
        }
    }
}
