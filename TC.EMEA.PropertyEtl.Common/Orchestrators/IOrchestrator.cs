﻿using TC.EMEA.PropertyEtl.Common.Handlers;
using TC.EMEA.PropertyEtl.Common.Models;
using TC.EMEA.PropertyEtl.Common.Requests;
using TC.EMEA.PropertyEtl.Common.Responses;

namespace TC.EMEA.PropertyEtl.Common.Orchestrators
{
    public interface IOrchestrator<in TOrchestrationRequest, out TOrchestrationResponse> : IHandler<TOrchestrationRequest, TOrchestrationResponse>
        where TOrchestrationRequest : OrchestrationRequest
        where TOrchestrationResponse : OrchestrationResponse
    {
    }

    

}