﻿namespace TC.EMEA.PropertyEtl.Common
{
    public class CosmosSettings
    {
        public string Endpoint { get; set; }

        public string AuthKey { get; set; }

        public string DataBaseName { get; set; }

        
    }
}