﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TC.EMEA.PropertyEtl.Common.Models
{
    public class S1Property : BaseEntity
    {
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public double? Lat { get; set; }
        public double? Lon { get; set; }
        public int? FloorCount { get; set; }
    }
}
