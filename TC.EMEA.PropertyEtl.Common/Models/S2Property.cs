﻿namespace TC.EMEA.PropertyEtl.Common.Models
{
    public class S2Property : BaseEntity
    {
        public string Name { get; set; }
        public string Address { get; set; }
        public string PostCode { get; set; }
        public double?[] Coordinates { get; set; }
        public int? FloorArea { get; set; }
    }
}