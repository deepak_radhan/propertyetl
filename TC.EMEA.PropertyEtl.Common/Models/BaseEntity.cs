﻿namespace TC.EMEA.PropertyEtl.Common.Models
{
    public class BaseEntity
    {
        public int? Id { get; set; }
    }
}