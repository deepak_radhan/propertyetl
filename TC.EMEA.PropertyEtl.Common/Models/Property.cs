﻿using System;
using TC.EMEA.PropertyEtl.Common.Enums;

namespace TC.EMEA.PropertyEtl.Common.Models
{
    public class Property
    {
        public Guid PropertyId { get; set; }
        public SourceSystem SourceSystem { get; set; }
        public string SourcePropertyId { get; set; }
        public string Name { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string PostCode { get; set; }
        public double? Latitude { get; set; }
        public double? Longitude { get; set; }
        public int? FloorCount { get; set; }
        public double? FloorArea { get; set; }
    }
}