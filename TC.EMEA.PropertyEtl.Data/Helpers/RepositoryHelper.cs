﻿using System;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using TC.EMEA.PropertyEtl.Common.Enums;
using TC.EMEA.PropertyEtl.Common.Models;
using TC.EMEA.PropertyEtl.Data.Repositories;

namespace TC.EMEA.PropertyEtl.Data.Helpers
{
    public class RepositoryHelper : Repository<Property>
    {
        public RepositoryHelper(DbContext context) : base(context)
        {
        }

        public async Task EnsureCreated()
        {
            var created = await this.Context.Database.EnsureCreatedAsync();
            System.Console.WriteLine(created ? "database created" : "database already exists");
        }
        public async Task CreateProperties()
        {
            var p1 = new Property()
            {
                PropertyId = Guid.Parse("153e8999-8b82-4962-8d8a-b52640f00d97"),
                SourceSystem = SourceSystem.SourceSystem1,
                Name = "The Shard",
                Address1 = "32 London Bridge St",
                Address2 = "",
                City = "London",
                Latitude = -0.87,
                Longitude = 1.56,
                FloorArea = 200,
                FloorCount = 100,
                SourcePropertyId = "234",
                PostCode = "EC1 2EE"

            };
            var p2 = new Property()
            {
                PropertyId = Guid.Parse("4633524c-f153-455a-a5d8-5bdbed74e435"),
                SourceSystem = SourceSystem.SourceSystem1,
                Name = "Canary Wharf Tower",
                Address1 = "40 Bank street",
                Address2 = "",
                City = "London",
                Latitude = -0.89,
                Longitude = 2.56,
                FloorArea = 200,
                FloorCount = 100,
                SourcePropertyId = "235",
                PostCode = "E14 5NR"
            };
            var p3 = new Property()
            {
                PropertyId = Guid.Parse("06c2ac88-c4f5-4e3b-949e-066a3c329293"),
                SourceSystem = SourceSystem.SourceSystem2,
                Name = "HSBC Building",
                Address1 = "20 Bank street",
                Address2 = "",
                City = "London",
                Latitude = -0.89,
                Longitude = 2.56,
                FloorArea = 200,
                FloorCount = 100,
                SourcePropertyId = "111",
                PostCode = "E14 7NH"
            };
            var p4 = new Property()
            {
                PropertyId = Guid.Parse("95e0402e-b5c1-4fa1-8478-680820d7e43c"),
                SourceSystem = SourceSystem.SourceSystem2,
                Name = "The Gherkin",
                Address1 = "StMarys Axe",
                Address2 = "",
                City = "London",
                Latitude = -0.89,
                Longitude = 2.56,
                FloorArea = 200,
                FloorCount = 100,
                SourcePropertyId = "898",
                PostCode = "EC5 8UI"
            };

            Context.Add(p1);
            Context.Add(p2);
            Context.Add(p3);
            Context.Add(p4);

            await Context.SaveChangesAsync();
        }
    }
}