﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using Microsoft.EntityFrameworkCore;
using TC.EMEA.PropertyEtl.Common.Data;
using TC.EMEA.PropertyEtl.Common.Specifications;

namespace TC.EMEA.PropertyEtl.Data.Repositories
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        protected readonly DbContext Context;
        
        private readonly DbSet<TEntity> _dbSet;
        
        public Repository(DbContext context)
        {
            Context = context;
            _dbSet = context.Set<TEntity>();
        }
        public IEnumerable<TEntity> Get(Func<TEntity, bool> predicate)
        {
            return _dbSet.Where(predicate).ToList();
        }

        public void Insert(TEntity entity)
        {
            _dbSet.Add(entity);
            Context.SaveChanges();
        }

        public void Update(TEntity entity)
        {
            Context.Entry(entity).State = EntityState.Modified;
            Context.SaveChanges();
        }

        public IEnumerable<TEntity> Find(AbstractSpecification<TEntity> specification)
        {
            return this.Get(specification.ToExpression().Compile());
        }
    }
}
