﻿using Microsoft.EntityFrameworkCore;
using TC.EMEA.PropertyEtl.Common.Models;

namespace TC.EMEA.PropertyEtl.Data.Contexts
{
    public class PropertiesContext : DbContext
    {
        public PropertiesContext(DbContextOptions<PropertiesContext> options) : base(options)
        {
        }
        public DbSet<Property> Properties { get; set; }
    }
}