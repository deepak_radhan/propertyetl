﻿using AutoMapper;
using FluentAssertions;
using System;
using TC.EMEA.PropertyEtl.Business.Tests.Handlers;
using TC.EMEA.PropertyEtl.Common;
using TC.EMEA.PropertyEtl.Common.Enums;
using TC.EMEA.PropertyEtl.Common.Mapping.Profiles;
using TC.EMEA.PropertyEtl.Common.Models;
using Xunit;

namespace TC.EMEA.PropertyEtl.Business.Tests.Converters
{
    public class MappingFixture : BaseHandlerFixture
    {
        private IMapper _mapper;
        public MappingFixture()
        {
            var mappingConfig = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<PropertyProfile>();
            });
            _mapper = mappingConfig.CreateMapper();
        }

        [Fact]
        public void Should_Map_Non_Null_Values_From_Source_To_Target()
        {
            var mappedProperty = _mapper.Map(this.IncomingProperty, this.PropertyInDataStore, opts => opts.Items[Constants.Mapping.OVERWRITE_SOURCE_INFO] = false);
            mappedProperty.Should().NotBeNull();
            mappedProperty.PropertyId.Should().Be(Guid.Parse("95e0402e-b5c1-4fa1-8478-680820d7e43c"));
            mappedProperty.SourceSystem.Should().Be(SourceSystem.SourceSystem2);
            mappedProperty.SourcePropertyId.Should().Be("898");
            mappedProperty.Name.Should().Be("The Gherkin building");
            mappedProperty.Address1.Should().Be("St Mary's Axe");
            mappedProperty.Address2.Should().Be("Liverpool street");
            mappedProperty.City.Should().Be("London");
            mappedProperty.Latitude.Should().Be(-0.99);
            mappedProperty.Longitude.Should().Be(1.78);
            mappedProperty.FloorArea.Should().Be(400);
            mappedProperty.FloorCount.Should().Be(50);
            mappedProperty.PostCode.Should().Be("EC5 8UK");
        }

        [Fact]
        public void Should_Not_Map_Null_Or_Empty_Values_From_Source_To_Target()
        {

            var incomingProperty = new Property()
            {
                PropertyId = Guid.Empty,
                SourceSystem = SourceSystem.SourceSystem2,
                SourcePropertyId = "898",
                Name = string.Empty,
                Address1 = "StMary's Axe 1",
                Address2 = "Liverpool street",
                City = "London",
                Latitude = -0.89,
                Longitude = 1.78,
                FloorArea = 400,
                FloorCount = 50,
                PostCode = "EC5 8UI"
            };

            var propertyInDataStore = new Property()
            {
                PropertyId = Guid.Parse("95e0402e-b5c1-4fa1-8478-680820d7e43c"),
                SourceSystem = SourceSystem.SourceSystem2,
                SourcePropertyId = "898", // id
                Name = "The Gherkin",
                Address1 = "StMary's Axe",
                Address2 = "",
                City = "London",
                Latitude = -0.89,
                Longitude = 2.56,
                FloorArea = 200,
                FloorCount = 100,
                PostCode = "EC5 8UI"
            };

            var mappedProperty = _mapper.Map(incomingProperty, propertyInDataStore, opts => opts.Items[Constants.Mapping.OVERWRITE_SOURCE_INFO] = false);
            mappedProperty.PropertyId.Should().Be(Guid.Parse("95e0402e-b5c1-4fa1-8478-680820d7e43c"));
            mappedProperty.SourceSystem.Should().Be(SourceSystem.SourceSystem2);
            mappedProperty.SourcePropertyId.Should().Be("898");
            mappedProperty.Address2.Should().Be("Liverpool street");
            mappedProperty.City.Should().Be("London");
            mappedProperty.Longitude.Should().Be(1.78);
            mappedProperty.FloorArea.Should().Be(400);
            mappedProperty.FloorCount.Should().Be(50);

            mappedProperty.Name.Should().Be("The Gherkin");
            mappedProperty.Address1.Should().Be("StMary's Axe 1");
            mappedProperty.PostCode.Should().Be("EC5 8UI");
            mappedProperty.Latitude.Should().Be(-0.89);
        }

        [Fact]
        public void Should_Map_From_Source_To_Target_With_Match_On_Name_Overwrite_Of_ID_And_SourceSystem()
        {
            var propertyInDataStore = new Property()
            {
                PropertyId = Guid.Parse("95e0402e-b5c1-4fa1-8478-680820d7e43c"),
                SourceSystem = SourceSystem.SourceSystem2,
                SourcePropertyId = "898", // id
                Name = "The Gherkin",
                Address1 = "StMary's Axe",
                Address2 = "",
                City = "London",
                Latitude = -0.89,
                Longitude = 2.56,
                FloorArea = null,
                FloorCount = 100,
                PostCode = "EC5 8UI"
            };

            var incomingProperty = new Property()
            {
                PropertyId = Guid.Empty,
                SourceSystem = SourceSystem.SourceSystem1,
                SourcePropertyId = "111",
                Name = "The Gherkin",
                Address1 = "St Mary's Axe 12",
                Address2 = "Liverpool street 12",
                City = "",
                Latitude = -0.89,
                Longitude = 1.78,
                FloorArea = 400,
                FloorCount = null,
                PostCode = "EC5 8UI"
            };

            var mappedProperty = _mapper.Map(incomingProperty, propertyInDataStore, opts => opts.Items[Constants.Mapping.OVERWRITE_SOURCE_INFO] = true);
            mappedProperty.PropertyId.Should().Be(Guid.Parse("95e0402e-b5c1-4fa1-8478-680820d7e43c"));
            mappedProperty.SourceSystem.Should().Be(SourceSystem.SourceSystem1);
            mappedProperty.SourcePropertyId.Should().Be("111");
            mappedProperty.Name.Should().Be("The Gherkin");
            mappedProperty.Address1.Should().Be("St Mary's Axe 12");
            mappedProperty.Address2.Should().Be("Liverpool street 12");
            mappedProperty.City.Should().Be("London");
            mappedProperty.Latitude.Should().Be(-0.89);
            mappedProperty.Longitude.Should().Be(1.78);
            mappedProperty.FloorArea.Should().Be(400);
            mappedProperty.FloorCount.Should().Be(100);
            mappedProperty.PostCode.Should().Be("EC5 8UI");
        }

        [Fact]
        public void Should_Map_From_Source_To_Target_With_Match_On_PostCode_Overwrite_Of_ID_And_SourceSystem()
        {
            var propertyInDataStore = new Property()
            {
                PropertyId = Guid.Parse("95e0402e-b5c1-4fa1-8478-680820d7e43c"),
                SourceSystem = SourceSystem.SourceSystem2,
                SourcePropertyId = "898", // id
                Name = "The Gherkin",
                Address1 = "StMary's Axe",
                Address2 = "",
                City = "London",
                Latitude = -0.89,
                Longitude = 2.56,
                FloorArea = null,
                FloorCount = 100,
                PostCode = "EC5 8UI"
            };

            var incomingProperty = new Property()
            {
                PropertyId = Guid.Empty,
                SourceSystem = SourceSystem.SourceSystem1,
                SourcePropertyId = "222",
                Name = "The Gherkin 12",
                Address1 = "St Mary's Axe 12",
                Address2 = "Liverpool street 12",
                City = "",
                Latitude = -0.89,
                Longitude = 1.78,
                FloorArea = 400,
                FloorCount = null,
                PostCode = "EC5 8UI"
            };

            var mappedProperty = _mapper.Map(incomingProperty, propertyInDataStore, opts => opts.Items[Constants.Mapping.OVERWRITE_SOURCE_INFO] = true);
            mappedProperty.PropertyId.Should().Be(Guid.Parse("95e0402e-b5c1-4fa1-8478-680820d7e43c"));
            mappedProperty.SourceSystem.Should().Be(SourceSystem.SourceSystem1);
            mappedProperty.SourcePropertyId.Should().Be("222");
            mappedProperty.Name.Should().Be("The Gherkin 12");
            mappedProperty.Address1.Should().Be("St Mary's Axe 12");
            mappedProperty.Address2.Should().Be("Liverpool street 12");
            mappedProperty.City.Should().Be("London");
            mappedProperty.Latitude.Should().Be(-0.89);
            mappedProperty.Longitude.Should().Be(1.78);
            mappedProperty.FloorArea.Should().Be(400);
            mappedProperty.FloorCount.Should().Be(100);
            mappedProperty.PostCode.Should().Be("EC5 8UI");
        }
    }
}
