﻿using FluentAssertions;
using TC.EMEA.PropertyEtl.Business.Handlers;
using TC.EMEA.PropertyEtl.Common.Enums;
using TC.EMEA.PropertyEtl.Common.Models;
using Xunit;

namespace TC.EMEA.PropertyEtl.Business.Tests.Handlers
{
    public class S2PropertyHandlerFixture
    {
        [Fact]
        public void Should_Parse_S1Property()
        {
            var s2Property = new S2Property()
            {
                Id = 2345,
                Name = "The Gherkin",
                Address = "30 St Mary Axe, London EC3A 8EP",
                PostCode = "EC3A 8EP",
                Coordinates = new double?[] { 0.0803, 51.5145 },
                FloorArea = 47950
            };

            S2PropertyHandler handler = new S2PropertyHandler();
            var property = handler.Execute(s2Property);
            property.Should().NotBeNull();
            property.SourceSystem.Should().Be(SourceSystem.SourceSystem2);
            property.SourcePropertyId.Should().Be("2345");
            property.Name.Should().Be("The Gherkin");
            property.Address1.Should().Be("30 St Mary Axe");
            property.Address2.Should().Be(null);
            property.City.Should().Be("London");
            property.PostCode.Should().Be("EC3A 8EP");
            property.FloorArea.Should().Be(47950);
            property.Latitude.Should().Be(0.0803);
            property.Longitude.Should().Be(51.5145);
        }
    }
}