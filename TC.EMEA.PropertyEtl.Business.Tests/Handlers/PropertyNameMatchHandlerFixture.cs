﻿using System;
using System.Collections.Generic;
using AutoMapper;
using FluentAssertions;
using Microsoft.Extensions.Logging.Abstractions;
using Moq;
using TC.EMEA.PropertyEtl.Business.Handlers;
using TC.EMEA.PropertyEtl.Common.Data;
using TC.EMEA.PropertyEtl.Common.Enums;
using TC.EMEA.PropertyEtl.Common.Models;
using TC.EMEA.PropertyEtl.Common.Responses;
using TC.EMEA.PropertyEtl.Common.Specifications;
using Xunit;

namespace TC.EMEA.PropertyEtl.Business.Tests.Handlers
{
    public class PropertyNameMatchHandlerFixture
    {
        private Mock<IMapper> _mockMapper;
        private Mock<IRepository<Property>> _mockRepository;
        private PropertyNameMatchHandler _handler;

        public PropertyNameMatchHandlerFixture()
        {
            _mockMapper = new Mock<IMapper>();
            _mockRepository = new Mock<IRepository<Property>>();
            _handler = new PropertyNameMatchHandler(_mockRepository.Object, _mockMapper.Object);
        }

        [Fact]
        public void Should_Fail_Fast_If_Name_Is_Null_Or_Empty()
        {
            var response = _handler.Execute(new Property() { SourcePropertyId = "123",Name = null});
            response.IsSuccess.Should().BeFalse();
        }

        [Fact]
        public void Should_Return_Failure_Response_If_No_Match_Is_Found()
        {
            var incomingProperty = new Property()
            {
                SourcePropertyId = "1234",
                Address1 = "12 Walbrook Street",
                PostCode = "EC1M 7YU",
                Name = "The Walbrook Building",
                City = "London",
                SourceSystem = SourceSystem.SourceSystem1,
                Address2 = string.Empty,
                Latitude = 1.0,
                Longitude = 2.0,
                FloorArea = 2000,
                FloorCount = 10,
                PropertyId = Guid.Empty
            };
            AbstractSpecification<Property> specification = null;
            _mockRepository.Setup(x => x.Find(It.IsAny<AbstractSpecification<Property>>()))
                .Returns((IEnumerable<Property>)null)
                .Callback<AbstractSpecification<Property>>(x =>
                {
                    specification = x;
                });

            var response = _handler.Execute(incomingProperty);

            response.Should().NotBeNull();
            response.IsSuccess.Should().BeFalse();
            specification.Should().BeOfType(typeof(PropertyNameMatchSpecification));
        }

        [Fact]
        public void Should_Map_Fields_Of_Property_In_Same_Source_If_Name_Matches()
        {
            var incomingProperty = new Property()
            {
                SourcePropertyId = "1234",
                Address1 = "12 Walbrook Street",
                PostCode = "EC1M 7YU",
                Name = "The Walbrook Building", // match
                City = "London",
                SourceSystem = SourceSystem.SourceSystem1,
                Address2 = string.Empty,
                Latitude = 21.0,
                Longitude = 22.0,
                FloorArea = 1000,
                FloorCount = 100,
                PropertyId = Guid.Empty
            };

            var propertyInDatastore = new Property()
            {
                SourcePropertyId = "1234",
                Address1 = "12 Walbrook Street South",
                PostCode = "EC1M 7YU",
                Name = "The Walbrook Building", //match
                City = "London",
                SourceSystem = SourceSystem.SourceSystem1,
                Address2 = string.Empty,
                Latitude = 1.0,
                Longitude = 2.0,
                FloorArea = 2000,
                FloorCount = 10,
                PropertyId = Guid.Empty
            };

            AbstractSpecification<Property> specification = null;
            int invocationOrder = 0;

            _mockRepository.Setup(x => x.Find(It.IsAny<AbstractSpecification<Property>>()))
                .Returns(new List<Property>() { propertyInDatastore })
                .Callback<AbstractSpecification<Property>>(x =>
                {
                    specification = x;
                    (++invocationOrder).Should().Be(1);
                });

            Property propertyToUpdate = null;
            _mockRepository.Setup(x => x.Update(It.IsAny<Property>()))
                .Callback<Property>(p =>
                {
                    propertyToUpdate = p;
                    (++invocationOrder).Should().Be(3);
                });

            Property sourceProperty = null;
            Property targetProperty = null;
            Action<IMappingOperationOptions<Property, Property>> mappingOptionAction = null;
            _mockMapper.Setup(x => x.Map(It.IsAny<Property>(), It.IsAny<Property>(),
                    It.IsAny<Action<IMappingOperationOptions<Property, Property>>>()))
                .Returns(It.IsAny<Property>())
                .Callback<Property, Property, Action<IMappingOperationOptions<Property, Property>>>((p1, p2, opt) =>
                {
                    sourceProperty = p1;
                    targetProperty = p2;
                    mappingOptionAction = opt;
                    (++invocationOrder).Should().Be(2);
                });
            //_handler = new PropertyNameMatchHandler(_mockRepository.Object,_mockMapper.Object,NullLogger<PropertyDataOrchestrator>.Instance);
            var result = _handler.Execute(incomingProperty);
            result.Should().NotBeNull();
            result.Should().BeOfType<PropertyOrchestrationResponse>();
            result.IsSuccess.Should().BeTrue();

            specification.Should().BeOfType<PropertyNameMatchSpecification>();
            invocationOrder.Should().Be(3);

            sourceProperty.Should().BeSameAs(incomingProperty);
            targetProperty.Should().BeSameAs(propertyInDatastore);

        }
    }
}
