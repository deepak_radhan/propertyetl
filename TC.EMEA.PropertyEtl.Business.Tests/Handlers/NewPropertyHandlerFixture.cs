﻿using System;
using AutoMapper;
using FluentAssertions;
using Moq;
using TC.EMEA.PropertyEtl.Business.Handlers;
using TC.EMEA.PropertyEtl.Common.Data;
using TC.EMEA.PropertyEtl.Common.Models;
using Xunit;

namespace TC.EMEA.PropertyEtl.Business.Tests.Handlers
{
    public class NewPropertyHandlerFixture : BaseHandlerFixture
    {
        private Mock<IMapper> _mockMapper;
        private Mock<IRepository<Property>> _mockRepository;
        private NewPropertyHandler _handler;

        public NewPropertyHandlerFixture()
        {
            _mockMapper = new Mock<IMapper>();
            _mockRepository = new Mock<IRepository<Property>>();
            _handler = new NewPropertyHandler(_mockRepository.Object, _mockMapper.Object);
        }

        [Fact]
        public void Should_Execute_Find_Map_Insert_In_Correct_Sequence()
        {
            _mockMapper.Setup(x => x.Map<Property>(It.IsAny<Property>(), It.IsAny<Action<IMappingOperationOptions<object, Property>>>()))
                .Returns(It.IsAny<Property>());
            _mockRepository.Setup(x => x.Insert(It.IsAny<Property>()));
            
            var response = _handler.Execute(this.IncomingProperty);

            response.Should().NotBeNull();
            response.IsSuccess.Should().BeTrue();

            _mockMapper.Verify(x => x.Map<Property>(It.IsAny<Property>(), It.IsAny<Action<IMappingOperationOptions<object, Property>>>()), Times.Once);
            _mockRepository.Verify(x => x.Insert(It.IsAny<Property>()), Times.Once);
        }

        [Fact]
        public void Should_Fail_Early_If_Property_Is_Not_Valid_For_Creation()
        {
            _mockMapper.Setup(x => x.Map<Property>(It.IsAny<Property>(), It.IsAny<Action<IMappingOperationOptions<object, Property>>>()))
                .Returns(It.IsAny<Property>());
            _mockRepository.Setup(x => x.Insert(It.IsAny<Property>()));

            var incomingProperty = new Property()
            {
                SourcePropertyId = "0",
                Address1 = "xxx",
                PostCode = "SSS",
                Name = "dddd"
            };

            var response = _handler.Execute(incomingProperty);
            response.IsSuccess.Should().BeFalse();

        }
    }
}
