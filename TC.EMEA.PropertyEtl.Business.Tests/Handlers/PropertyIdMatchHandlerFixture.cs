﻿using AutoMapper;
using FluentAssertions;
using Moq;
using System;
using System.Collections.Generic;
using Microsoft.Extensions.Logging.Abstractions;
using TC.EMEA.PropertyEtl.Business.Handlers;
using TC.EMEA.PropertyEtl.Common.Data;
using TC.EMEA.PropertyEtl.Common.Models;
using TC.EMEA.PropertyEtl.Common.Specifications;
using Xunit;

namespace TC.EMEA.PropertyEtl.Business.Tests.Handlers
{
    public class PropertyIdMatchHandlerFixture : BaseHandlerFixture
    {
        private Mock<IMapper> _mockMapper;
        private Mock<IRepository<Property>> _mockRepository;
        private PropertyIdMatchHandler _handler;
        public PropertyIdMatchHandlerFixture()
        {
            _mockMapper = new Mock<IMapper>();
            _mockRepository = new Mock<IRepository<Property>>();
            _handler = new PropertyIdMatchHandler(_mockRepository.Object, _mockMapper.Object);
        }

        [Fact]
        public void Should_Fail_Fast_If_Id_Is_Null_Or_Empty()
        {
            var response = _handler.Execute(new Property() {SourcePropertyId = null});
            response.IsSuccess.Should().BeFalse();
        }

        [Fact]
        public void Should_Fail_Fast_If_Id_Is_Stringified_Zero()
        {
            var response = _handler.Execute(new Property() { SourcePropertyId = "0" });
            response.IsSuccess.Should().BeFalse();
        }

        [Fact]
        public void Should_Execute_Find_Map_Update_In_Correct_Sequence()
        {
            int invocationOrder = 0;
            AbstractSpecification<Property> specification = null;

            Property sourceProperty = null;
            Property targetProperty= null;
            Action<IMappingOperationOptions<Property, Property>> mappingOptionAction = null;

            Property propertyToUpdate = null;

            _mockRepository.Setup(x => x.Find(It.IsAny<AbstractSpecification<Property>>()))
            .Returns(new List<Property>() { this.PropertyInDataStore })                
            .Callback<AbstractSpecification<Property>>(x =>
            {
                specification = x;
                (++invocationOrder).Should().Be(1);
            });

            _mockMapper.Setup(x => x.Map(It.IsAny<Property>(), It.IsAny<Property>(), It.IsAny<Action<IMappingOperationOptions<Property, Property>>>()))
                .Returns(It.IsAny<Property>())
                .Callback<Property, Property, Action<IMappingOperationOptions<Property, Property>>>((p1, p2, opt) =>
                {
                    sourceProperty = p1;
                    targetProperty = p2;
                    mappingOptionAction = opt;
                    (++invocationOrder).Should().Be(2);
                });

            _mockRepository.Setup(x => x.Update(It.IsAny<Property>()))
                .Callback<Property>(p => {
                    propertyToUpdate = p;
                    (++invocationOrder).Should().Be(3);
                });  
            
            
            var response = _handler.Execute(this.IncomingProperty);

            response.Should().NotBeNull();
            response.IsSuccess.Should().BeTrue();

            _mockRepository.Verify(x => x.Find(It.IsAny<AbstractSpecification<Property>>()), Times.Once);
            specification.Should().BeOfType<PropertyIdMatchSpecification>();

            _mockMapper.Verify(x => x.Map(It.IsAny<Property>(), It.IsAny<Property>(), It.IsAny<Action<IMappingOperationOptions<Property, Property>>>()), Times.Once);
            sourceProperty.Should().BeSameAs(this.IncomingProperty);
            targetProperty.Should().BeSameAs(this.PropertyInDataStore);

            _mockRepository.Verify(x => x.Update(It.IsAny<Property>()), Times.Once);  
            _mockRepository.VerifyNoOtherCalls();
        }
    }
}
