﻿using FluentAssertions;
using TC.EMEA.PropertyEtl.Business.Handlers;
using TC.EMEA.PropertyEtl.Common.Enums;
using TC.EMEA.PropertyEtl.Common.Models;
using Xunit;

namespace TC.EMEA.PropertyEtl.Business.Tests.Handlers
{
    public class S1PropertyHandlerFixture
    {
        [Fact]
        public void Should_Parse_S1Property()
        {
            var s1Property = new S1Property()
            {
                Address1 = "The Shard, 32 London Bridge St",
                Id = 1234,
                Address2 = "London SE1 9SG",
                Lon = 51.5045,
                Lat = 0.0865,
                FloorCount = 95
            };

            S1PropertyHandler handler = new S1PropertyHandler();
            var property =  handler.Execute(s1Property);
            property.Should().NotBeNull();
            property.SourceSystem.Should().Be(SourceSystem.SourceSystem1);
            property.SourcePropertyId.Should().Be("1234");
            property.Name.Should().Be("The Shard");
            property.Address1.Should().Be("32 London Bridge St");
            property.Address2.Should().Be(null);
            property.City.Should().Be("London");
            property.PostCode.Should().Be("SE1 9SG");
            property.FloorCount.Should().Be(95);
            property.Latitude.Should().Be(0.0865);
            property.Longitude.Should().Be(51.5045);
        }

        [Fact]
        public void Should_Parse_S1Property_With_A_Single_Address1_Value()
        {
            var s1Property = new S1Property()
            {
                Address1 = "32 London Bridge St",
                Id = 1234,
                Address2 = "London SE1 9SG",
                Lon = 51.5045,
                Lat = 0.0865,
                FloorCount = 95
            };

            S1PropertyHandler handler = new S1PropertyHandler();
            var property = handler.Execute(s1Property);
            property.Should().NotBeNull();
            property.SourceSystem.Should().Be(SourceSystem.SourceSystem1);
            property.SourcePropertyId.Should().Be("1234");
            property.Name.Should().Be("");
            property.Address1.Should().Be("32 London Bridge St");
            property.Address2.Should().Be(null);
            property.City.Should().Be("London");
            property.PostCode.Should().Be("SE1 9SG");
            property.FloorCount.Should().Be(95);
            property.Latitude.Should().Be(0.0865);
            property.Longitude.Should().Be(51.5045);
        }

        [Fact]
        public void Should_Parse_S1Property_With_A_Single_Address2_Value()
        {
            var s1Property = new S1Property()
            {
                Address1 = "32 London Bridge St",
                Id = 100,
                Address2 = "Canary Wharf",
                Lon = 51.5045,
                Lat = 0.0865,
                FloorCount = 95
            };

            S1PropertyHandler handler = new S1PropertyHandler();
            var property = handler.Execute(s1Property);
            property.Should().NotBeNull();
            property.SourceSystem.Should().Be(SourceSystem.SourceSystem1);
            property.SourcePropertyId.Should().Be("100");
            property.Name.Should().Be("");
            property.Address1.Should().Be("32 London Bridge St");
            property.Address2.Should().Be("Canary Wharf");
            property.City.Should().Be(string.Empty);
            property.PostCode.Should().Be(string.Empty);
            property.FloorCount.Should().Be(95);
            property.Latitude.Should().Be(0.0865);
            property.Longitude.Should().Be(51.5045);
        }
    }
}