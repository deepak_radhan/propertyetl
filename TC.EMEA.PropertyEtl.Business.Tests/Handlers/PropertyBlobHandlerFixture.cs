﻿using System.IO;
using System.Text;
using FluentAssertions;
using Microsoft.Extensions.Logging.Abstractions;
using TC.EMEA.PropertyEtl.Business.Handlers;
using Xunit;

namespace TC.EMEA.PropertyEtl.Business.Tests.Handlers
{
    public class PropertyBlobHandlerFixture
    {
        private PropertyBlobStreamHandler _blobStreamHandler;
        public PropertyBlobHandlerFixture()
        {
            _blobStreamHandler = new PropertyBlobStreamHandler(NullLogger.Instance);
        }

        [Fact]
        public void Should_Parse_S2Property_From_Request_As_Full_Object()
        {
            var s2PropertyString = @"{
            ""id"": 2345,
            ""name"": ""The Gherkin"",
            ""address"": ""30 St Mary Axe, London EC3A 8EP"",
            ""postcode"": ""EC3A 8EP"",
            ""coordinates"": [0.0803, 51.5145],
            ""floorarea"": 47950
            }";

            using var stream = new MemoryStream(Encoding.UTF8.GetBytes(s2PropertyString));
            var s2Property = _blobStreamHandler.Execute(stream);
            s2Property.Should().NotBeNull();
            s2Property.Name.Should().Be("The Gherkin");
            s2Property.PostCode.Should().Be("EC3A 8EP");
            s2Property.Address.Should().Be("30 St Mary Axe, London EC3A 8EP");
            s2Property.Coordinates.Should().BeEquivalentTo(new double[]{ 0.0803, 51.5145 });
            s2Property.FloorArea.Should().Be(47950);
            s2Property.Id.Should().Be(2345);

        }

        [Fact]
        public void Should_Parse_S1Property_From_Request_As_Partial_Object_With_Id()
        {
            var s2PropertyString = @"{
                ""id"": 1234,                
                ""floorarea"": 10000
            }";

            using var stream = new MemoryStream(Encoding.UTF8.GetBytes(s2PropertyString));
            var s2Property = _blobStreamHandler.Execute(stream);
            s2Property.Should().NotBeNull();
            s2Property.Id.Should().Be(1234);
            s2Property.FloorArea.Should().Be(10000);
        }

        [Fact]
        public void Should_Parse_S1Property_From_Request_As_Partial_Object_WithOut_Id()
        {
            var s2PropertyString = @"{                
                ""address"": ""30 St Mary Axe, London EC3A 8EP"",
                ""postcode"": ""EC3A 8EP"",             
            }";
           
            using var stream = new MemoryStream(Encoding.UTF8.GetBytes(s2PropertyString));
            var s2Property = _blobStreamHandler.Execute(stream);
            s2Property.Should().NotBeNull();
            s2Property.Address.Should().Be("30 St Mary Axe, London EC3A 8EP");
            s2Property.PostCode.Should().Be("EC3A 8EP");
        }

        [Fact]
        public void Should_Parse_Malformed_Json()
        {
            var s2PropertyString = @"{                
                ""address"": ""30 St Mary Axe, London EC3A 8EP"",
                ""postcode"": ""EC3A 8EP"",  
                ""floorarea"": ""Foo bar"",
                ""coordinates"": ""Bar baz"",
            }";

            using var stream = new MemoryStream(Encoding.UTF8.GetBytes(s2PropertyString));
            var s2Property = _blobStreamHandler.Execute(stream);
            s2Property.Should().NotBeNull();
            s2Property.Address.Should().Be("30 St Mary Axe, London EC3A 8EP");
            s2Property.PostCode.Should().Be("EC3A 8EP");
            s2Property.FloorArea.Should().BeNull();
            s2Property.Coordinates.Should().BeNull();

        }

        [Fact]
        public void Should_Parse_Unknown_JSON_Structure()
        {
            var s2PropertyString = @"{                
                ""ddd"": ""30 St Mary Axe, London EC3A 8EP"",
                ""ccc"": ""EC3A 8EP"",  
                ""www"": ""Foo bar"",
                ""rrrr"": ""Bar baz"",
            }";

            using var stream = new MemoryStream(Encoding.UTF8.GetBytes(s2PropertyString));
            var s2Property = _blobStreamHandler.Execute(stream);
            s2Property.Should().NotBeNull();
        }

        [Fact]
        public void Should_Parse_Empty_Object()
        {
            var s2PropertyString = @"{}";

            using var stream = new MemoryStream(Encoding.UTF8.GetBytes(s2PropertyString));
            var s2Property = _blobStreamHandler.Execute(stream);
            s2Property.Should().NotBeNull();
        }
    }
}