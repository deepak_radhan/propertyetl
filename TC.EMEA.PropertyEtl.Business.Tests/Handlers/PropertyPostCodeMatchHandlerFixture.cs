﻿using AutoMapper;
using FluentAssertions;
using Microsoft.Extensions.Logging.Abstractions;
using Moq;
using TC.EMEA.PropertyEtl.Business.Handlers;
using TC.EMEA.PropertyEtl.Business.Orchestrators;
using TC.EMEA.PropertyEtl.Common.Data;
using TC.EMEA.PropertyEtl.Common.Models;
using Xunit;

namespace TC.EMEA.PropertyEtl.Business.Tests.Handlers
{
    public class PropertyPostCodeMatchHandlerFixture
    {

        private Mock<IMapper> _mockMapper;
        private Mock<IRepository<Property>> _mockRepository;
        private PropertyPostCodeMatchHandler _handler;

        public PropertyPostCodeMatchHandlerFixture()
        {
            _mockMapper = new Mock<IMapper>();
            _mockRepository = new Mock<IRepository<Property>>();
            _handler = new PropertyPostCodeMatchHandler(_mockRepository.Object, _mockMapper.Object);
        }


        [Fact]
        public void Should_Fail_Fast_If_PostCode_Is_Null_Or_Empty()
        {
            var response = _handler.Execute(new Property() { SourcePropertyId = "123",PostCode = null});
            response.IsSuccess.Should().BeFalse();
        }
    }
}