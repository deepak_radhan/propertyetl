﻿using System.IO;
using System.Text;
using FluentAssertions;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging.Abstractions;
using TC.EMEA.PropertyEtl.Business.Handlers;
using TC.EMEA.PropertyEtl.Common.Models;
using Xunit;

namespace TC.EMEA.PropertyEtl.Business.Tests.Handlers
{
    public class PropertyHttpMessageHandlerFixture
    {
        private PropertyHttpMessageHandler<S1Property> _converterHandler;
        public PropertyHttpMessageHandlerFixture()
        {
            _converterHandler = new PropertyHttpMessageHandler<S1Property>(NullLogger.Instance);
        }

        [Fact]
        public void Should_Parse_S1Property_From_Request_As_Full_Object()
        {
            var s1PropertyString = @"{
                ""id"": 1234,
                ""address1"": ""The Shard, 32 London Bridge St"",
                ""address2"": ""London SE1 9SG"",
                ""lat"": -0.0865,
                ""lon"": 51.5045,
                ""floorcount"": 95
            }";

            var httpContext = new DefaultHttpContext();
            using var stream = new MemoryStream(Encoding.UTF8.GetBytes(s1PropertyString));
            httpContext.Request.Body = stream;
            var s1Property = _converterHandler.Execute(httpContext.Request);
            s1Property.Should().NotBeNull();
            s1Property.Id.Should().Be(1234);
            s1Property.Address1.Should().Be("The Shard, 32 London Bridge St");
            s1Property.Address2.Should().Be("London SE1 9SG");
            s1Property.Lat.Should().Be(-0.0865);
            s1Property.Lon.Should().Be(51.5045);
            s1Property.FloorCount.Should().Be(95);
        }

        [Fact]
        public void Should_Parse_S1Property_From_Request_As_Partial_Object_With_Id()
        {
            var s1PropertyString = @"{
                ""id"": 1234,                
                ""floorcount"": 100
            }";

            var httpContext = new DefaultHttpContext();
            using var stream = new MemoryStream(Encoding.UTF8.GetBytes(s1PropertyString));
            httpContext.Request.Body = stream;
            var s1Property = _converterHandler.Execute(httpContext.Request);
            s1Property.Should().NotBeNull();
            s1Property.Id.Should().Be(1234);
            s1Property.FloorCount.Should().Be(100);
        }

        [Fact]
        public void Should_Parse_S1Property_From_Request_As_Partial_Object_WithOut_Id()
        {
            var s1PropertyString = @"{                
                ""address1"": ""The Shard, 32 London Bridge St"",
                ""address2"": ""London SE1 9SG"",                
            }";

            var httpContext = new DefaultHttpContext();
            using var stream = new MemoryStream(Encoding.UTF8.GetBytes(s1PropertyString));
            httpContext.Request.Body = stream;
            var s1Property = _converterHandler.Execute(httpContext.Request);
            s1Property.Should().NotBeNull();
            s1Property.Address1.Should().Be("The Shard, 32 London Bridge St");
            s1Property.Address2.Should().Be("London SE1 9SG");
        }

        [Fact]
        public void Should_Parse_Malformed_Json_With_No_Ids()
        {
            var s1PropertyString = @"{                
                ""address1"": ""The Shard, 32 London Bridge St"",
                ""address2"": ""London SE1 9SG"",  
                ""floorcount"": ""John""
            }";

            var httpContext = new DefaultHttpContext();
            using var stream = new MemoryStream(Encoding.UTF8.GetBytes(s1PropertyString));
            httpContext.Request.Body = stream;
            var s1Property = _converterHandler.Execute(httpContext.Request);

            s1Property.Should().NotBeNull();
            s1Property.Address1.Should().Be("The Shard, 32 London Bridge St");
            s1Property.Address2.Should().Be("London SE1 9SG");
            s1Property.FloorCount.Should().Be(null);
        }

        [Fact]
        public void Should_Parse_Unknown_JSON_Structure()
        {
            var s1PropertyString = @"{
                ""xxx"": 1234,
                ""uuu"": ""The Shard, 32 London Bridge St"",
                ""aaa"": ""London SE1 9SG"",
                ""bbb"": -0.0865,
                ""ddd"": 51.5045,
                ""rrr"": 95
            }";

            var httpContext = new DefaultHttpContext();
            using var stream = new MemoryStream(Encoding.UTF8.GetBytes(s1PropertyString));
            httpContext.Request.Body = stream;
            var s1Property = _converterHandler.Execute(httpContext.Request);
            s1Property.Should().NotBeNull();
        }
        
        [Fact]
        public void Should_Parse_Empty_Object()
        {
            var s1PropertyString = @"{}";

            var httpContext = new DefaultHttpContext();
            using var stream = new MemoryStream(Encoding.UTF8.GetBytes(s1PropertyString));
            httpContext.Request.Body = stream;
            var s1Property = _converterHandler.Execute(httpContext.Request);
            s1Property.Should().NotBeNull();
        }

    }
}