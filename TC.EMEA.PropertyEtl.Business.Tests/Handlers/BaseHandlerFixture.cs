﻿using System;
using TC.EMEA.PropertyEtl.Common.Enums;
using TC.EMEA.PropertyEtl.Common.Models;

namespace TC.EMEA.PropertyEtl.Business.Tests.Handlers
{
    public class BaseHandlerFixture
    {
        public Property PropertyInDataStore { get; set; } = new Property()
        {
            PropertyId = Guid.Parse("95e0402e-b5c1-4fa1-8478-680820d7e43c"),
            SourceSystem = SourceSystem.SourceSystem2,
            SourcePropertyId = "898", // id
            Name = "The Gherkin",
            Address1 = "StMary's Axe",
            Address2 = "",
            City = "London",
            Latitude = -0.89,
            Longitude = 2.56,
            FloorArea = 200,
            FloorCount = 100,
            PostCode = "EC5 8UI"
        };

        public Property IncomingProperty { get; set; } = new Property()
        {
            PropertyId = Guid.Empty, // will not have a value
            SourceSystem = SourceSystem.SourceSystem2,
            SourcePropertyId = "898", // id match
            Name = "The Gherkin building",
            Address1 = "St Mary's Axe",
            Address2 = "Liverpool street",
            City = "London",
            Latitude = -0.99,
            Longitude = 1.78,
            FloorArea = 400,
            FloorCount = 50,
            PostCode = "EC5 8UK"
        };
    }
}
