﻿using System;
using System.IO;
using System.Text;
using Microsoft.Extensions.Logging.Abstractions;
using Moq;
using TC.EMEA.PropertyEtl.Common.Enums;
using TC.EMEA.PropertyEtl.Common.Handlers;
using TC.EMEA.PropertyEtl.Common.Models;
using TC.EMEA.PropertyEtl.Common.Orchestrators;
using TC.EMEA.PropertyEtl.Common.Requests;
using TC.EMEA.PropertyEtl.Common.Responses;
using TC.EMEA.PropertyEtl.Functions;
using Xunit;

namespace TC.EMEA.PropertyEtl.Business.Tests.Functions
{
    public class BlobTriggerFixture
    {
         Mock<IHandler<Stream, Property>> _streamRequestHandler;
         Mock<IOrchestrator<PropertyOrchestrationRequest, PropertyOrchestrationResponse>> _propertyDataOrchestrator;
         public BlobTriggerFixture()
         {
             _streamRequestHandler = new Mock<IHandler<Stream, Property>>();
             _propertyDataOrchestrator = new Mock<IOrchestrator<PropertyOrchestrationRequest, PropertyOrchestrationResponse>>();
         }

         [Fact]
         public void Should_Return_From_Function_If_Message_Is_Invalid()
         {
             var s2PropertyString = @"{
            ""id"": 2345,
            ""name"": ""The Gherkin"",
            ""address"": ""30 St Mary Axe, London EC3A 8EP"",
            ""postcode"": ""EC3A 8EP"",
            ""coordinates"": [0.0803, 51.5145],
            ""floorarea"": 47950
            }";

            var property = new Property()
             {
                 FloorCount = null,
                 Address2 = null,
                 SourcePropertyId = null,
                 Address1 = null,
                 Name = null,
                 PostCode = null,
                 FloorArea = null,
                 City = null,
                 Latitude = null,
                 SourceSystem = default(SourceSystem),
                 Longitude = null,
                 PropertyId = Guid.Empty
             };
            using var stream = new MemoryStream(Encoding.UTF8.GetBytes(s2PropertyString));
            _streamRequestHandler.Setup(x => x.Execute(It.IsAny<Stream>())).Returns(property);
             _propertyDataOrchestrator.Setup(x => x.Execute(It.IsAny<PropertyOrchestrationRequest>()))
                 .Returns(It.IsAny<PropertyOrchestrationResponse>());
             BlobTriggers blobFunction = new BlobTriggers(_streamRequestHandler.Object, _propertyDataOrchestrator.Object);
             blobFunction.Run(stream, It.IsAny<string>(), NullLogger.Instance);

             _streamRequestHandler.Verify(x => x.Execute(It.IsAny<Stream>()),Times.Once);
             _propertyDataOrchestrator.Verify(x => x.Execute(It.IsAny<PropertyOrchestrationRequest>()),Times.Never);

         }

         [Fact]
         public void Should_Execute_Function_If_Message_Is_Valid()
         {
             var s2PropertyString = @"{
            ""id"": 2345,
            ""name"": ""The Gherkin"",
            ""address"": ""30 St Mary Axe, London EC3A 8EP"",
            ""postcode"": ""EC3A 8EP"",
            ""coordinates"": [0.0803, 51.5145],
            ""floorarea"": 47950
            }";

             var property = new Property()
             {
                 FloorCount = 10,
                 Address2 = null,
                 SourcePropertyId = "111111",
                 Address1 = null,
                 Name = null,
                 PostCode = null,
                 FloorArea = null,
                 City = null,
                 Latitude = null,
                 SourceSystem = default(SourceSystem),
                 Longitude = null,
                 PropertyId = Guid.Empty
             };
             using var stream = new MemoryStream(Encoding.UTF8.GetBytes(s2PropertyString));
             _streamRequestHandler.Setup(x => x.Execute(It.IsAny<Stream>())).Returns(property);
             _propertyDataOrchestrator.Setup(x => x.Execute(It.IsAny<PropertyOrchestrationRequest>()))
                 .Returns(PropertyOrchestrationResponse.SuccessResponse);
             BlobTriggers blobFunction = new BlobTriggers(_streamRequestHandler.Object, _propertyDataOrchestrator.Object);
             blobFunction.Run(stream, It.IsAny<string>(), NullLogger.Instance);

             _streamRequestHandler.Verify(x => x.Execute(It.IsAny<Stream>()), Times.Once);
             _propertyDataOrchestrator.Verify(x => x.Execute(It.IsAny<PropertyOrchestrationRequest>()), Times.Once);

         }
    }
}