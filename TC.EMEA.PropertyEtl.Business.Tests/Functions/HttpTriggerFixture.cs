﻿using System;
using FluentAssertions;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging.Abstractions;
using Moq;
using TC.EMEA.PropertyEtl.Common.Enums;
using TC.EMEA.PropertyEtl.Common.Handlers;
using TC.EMEA.PropertyEtl.Common.Models;
using TC.EMEA.PropertyEtl.Common.Orchestrators;
using TC.EMEA.PropertyEtl.Common.Requests;
using TC.EMEA.PropertyEtl.Common.Responses;
using TC.EMEA.PropertyEtl.Functions;
using Xunit;

namespace TC.EMEA.PropertyEtl.Business.Tests.Functions
{
    public class HttpTriggerFixture
    {
        readonly Mock<IHandler<HttpRequest, Property>> _mockHttpRequestHandler;
        readonly Mock<IOrchestrator<PropertyOrchestrationRequest, PropertyOrchestrationResponse>> _mockPropertyDataOrchestrator;

        public HttpTriggerFixture()
        {
            _mockHttpRequestHandler = new Mock<IHandler<HttpRequest, Property>>();
            _mockPropertyDataOrchestrator = new Mock<IOrchestrator<PropertyOrchestrationRequest, PropertyOrchestrationResponse>>();
        }

        [Fact]
        public void Should_Return_Http400_If_Message_Is_Invalid()
        {
            var property = new Property()
            {
                FloorCount = null,
                Address2 = null,
                SourcePropertyId = null,
                Address1 = null,
                Name = null,
                PostCode = null,
                FloorArea = null,
                City = null,
                Latitude = null,
                SourceSystem = default(SourceSystem),
                Longitude = null,
                PropertyId = Guid.Empty
            };
            _mockHttpRequestHandler.Setup(x => x.Execute(It.IsAny<HttpRequest>())).Returns(property);
            var httpContext = new DefaultHttpContext();
            HttpTriggers httpFunction = new HttpTriggers(_mockHttpRequestHandler.Object, _mockPropertyDataOrchestrator.Object);
            var response = httpFunction.Get(httpContext.Request, NullLogger.Instance);
            response.Should().BeOfType<BadRequestObjectResult>();
        }

        [Fact]
        public void Should_Return_OkResult_If_Message_Processing_Succeeds()
        {
            var property = new Property()
            {
                FloorCount = null,
                Address2 = null,
                SourcePropertyId = "123",
                Address1 = null,
                Name = null,
                PostCode = null,
                FloorArea = null,
                City = null,
                Latitude = null,
                SourceSystem = default(SourceSystem),
                Longitude = null,
                PropertyId = Guid.Empty
            };
            _mockHttpRequestHandler.Setup(x => x.Execute(It.IsAny<HttpRequest>())).Returns(property);
            _mockPropertyDataOrchestrator.Setup(x => x.Execute(It.IsAny<PropertyOrchestrationRequest>()))
                .Returns(PropertyOrchestrationResponse.SuccessResponse);
            var httpContext = new DefaultHttpContext();
            HttpTriggers httpFunction = new HttpTriggers(_mockHttpRequestHandler.Object, _mockPropertyDataOrchestrator.Object);
            var response = httpFunction.Get(httpContext.Request, NullLogger.Instance);
            response.Should().BeOfType<OkResult>();
        }

        [Fact]
        public void Should_Return_InternalServerErrorResult_If_Message_Processing_Fails()
        {
            var property = new Property()
            {
                FloorCount = 10,
                Address2 = null,
                SourcePropertyId = "123",
                Address1 = null,
                Name = null,
                PostCode = null,
                FloorArea = null,
                City = null,
                Latitude = null,
                SourceSystem = default(SourceSystem),
                Longitude = null,
                PropertyId = Guid.Empty
            };
            _mockHttpRequestHandler.Setup(x => x.Execute(It.IsAny<HttpRequest>())).Returns(property);
            _mockPropertyDataOrchestrator.Setup(x => x.Execute(It.IsAny<PropertyOrchestrationRequest>()))
                .Returns(PropertyOrchestrationResponse.FailureResponse);
            var httpContext = new DefaultHttpContext();
            HttpTriggers httpFunction = new HttpTriggers(_mockHttpRequestHandler.Object, _mockPropertyDataOrchestrator.Object);
            var response = httpFunction.Get(httpContext.Request, NullLogger.Instance);
            response.Should().BeOfType<BadRequestObjectResult>();
        }

    }
}