﻿using FluentAssertions;
using System;
using TC.EMEA.PropertyEtl.Common.Enums;
using TC.EMEA.PropertyEtl.Common.Models;
using TC.EMEA.PropertyEtl.Common.Specifications;
using Xunit;

namespace TC.EMEA.PropertyEtl.Business.Tests.Specifications
{
    public class PropertyNameMatchSpecificationFixture
    {
        [Fact]
        public void Should_Return_True_If_Specification_Matches()
        {
            Property propertyInDataStore = new Property()
            {
                PropertyId = Guid.Parse("95e0402e-b5c1-4fa1-8478-680820d7e43c"),
                SourceSystem = SourceSystem.SourceSystem2,
                SourcePropertyId = "898", // id
                Name = "The Gherkin",
                Address1 = "StMary's Axe",
                Address2 = "",
                City = "London",
                Latitude = -0.89,
                Longitude = 2.56,
                FloorArea = 200,
                FloorCount = 100,
                PostCode = "EC5 8UI"
            };

            Property incomingProperty = new Property()
            {
                PropertyId = Guid.Empty, 
                SourceSystem = SourceSystem.SourceSystem2,
                SourcePropertyId = "8981",
                Name = "The Gherkin",
                Address1 = "St Mary's Axe",
                Address2 = "Liverpool street",
                City = "London",
                Latitude = -0.99,
                Longitude = 1.78,
                FloorArea = 400,
                FloorCount = 50,
                PostCode = "EC5 8UK"
            };
            PropertyNameMatchSpecification spec = new PropertyNameMatchSpecification(incomingProperty);
            spec.IsSatisfiedBy(propertyInDataStore).Should().BeTrue();
        }

        [Fact]
        public void Should_Return_False_If_Specification_Does_Not_Match()
        {
            Property propertyInDataStore = new Property()
            {
                PropertyId = Guid.Parse("95e0402e-b5c1-4fa1-8478-680820d7e43c"),
                SourceSystem = SourceSystem.SourceSystem2,
                SourcePropertyId = "898",
                Name = "The Gherkin",
                Address1 = "StMary's Axe",
                Address2 = "",
                City = "London",
                Latitude = -0.89,
                Longitude = 2.56,
                FloorArea = 200,
                FloorCount = 100,
                PostCode = "EC5 8UI"
            };

            Property incomingProperty = new Property()
            {
                PropertyId = Guid.Empty,
                SourceSystem = SourceSystem.SourceSystem2,
                SourcePropertyId = "8981",
                Name = "The Gherkin building",
                Address1 = "St Mary's Axe",
                Address2 = "Liverpool street",
                City = "London",
                Latitude = -0.99,
                Longitude = 1.78,
                FloorArea = 400,
                FloorCount = 50,
                PostCode = "EC5 8UK"
            };
            PropertyNameMatchSpecification spec = new PropertyNameMatchSpecification(incomingProperty);
            spec.IsSatisfiedBy(propertyInDataStore).Should().BeFalse();
        }
    }
}
