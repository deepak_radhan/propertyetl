﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TC.EMEA.PropertyEtl.ClientApp.Models;
using TC.EMEA.PropertyEtl.ClientApp.Services;

namespace TC.EMEA.PropertyEtl.ClientApp.Controllers
{
    public class S1PropertyController : Controller
    {
        private readonly HttpGatewayService _gatewayService;

        public S1PropertyController(HttpGatewayService _gatewayService)
        {
            this._gatewayService = _gatewayService;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpPost, ValidateAntiForgeryToken]
        public async Task<IActionResult> Index(S1PropertyModel model)
        {
            try
            {
                var response = await this._gatewayService.PostMessage(model);
            }
            catch (Exception exception)
            {
                // log 
                
            }
            return RedirectToAction("Index");
        }
    }
}
