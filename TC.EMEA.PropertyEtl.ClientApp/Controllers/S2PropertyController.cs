﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Azure.Storage.Blobs;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Primitives;
using TC.EMEA.PropertyEtl.ClientApp.Models;

namespace TC.EMEA.PropertyEtl.ClientApp.Controllers
{
    public class S2PropertyController : Controller
    {
        private string connection = "connection info of storage";
           

        private string containerName = "drop folder name";
        public IActionResult Index()
        {
            return View(new S2PropertyViewModel());
        }

        [HttpPost]
        public async Task <IActionResult> Index(S2PropertyViewModel viewModel)
        {
            if(viewModel == null) return new BadRequestObjectResult("Empty view model");
            if(viewModel.S2PropertyFile == null) return new BadRequestObjectResult("No file to upload");

            try
            {
                var fileName = $"{viewModel.S2PropertyFile.FileName}{Path.GetRandomFileName()}";
                BlobServiceClient client = new BlobServiceClient(connection);
                BlobContainerClient blobContainerClient = client.GetBlobContainerClient(containerName);
                BlobClient blobClient = blobContainerClient.GetBlobClient(fileName);
                using var stream = viewModel.S2PropertyFile.OpenReadStream();
                await blobClient.UploadAsync(stream);
                return RedirectToAction("Index");
            }
            catch (Exception exception)
            {
                // log
                return RedirectToAction("Index");
                
            }
        }
    }
}
