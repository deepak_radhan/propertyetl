﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using TC.EMEA.PropertyEtl.ClientApp.Models;

namespace TC.EMEA.PropertyEtl.ClientApp.Services
{
    public class HttpGatewayService
    {
        public HttpClient Client { get; }

        public HttpGatewayService(HttpClient client)
        {
            client.BaseAddress = new Uri("Http function base address");
            Client = client;
        }

        public async Task<HttpResponseMessage> PostMessage(S1PropertyModel propertyModel)
        {
            var propertyString = JsonConvert.SerializeObject(propertyModel);
            HttpContent content = new StringContent(propertyString);
            var response = await this.Client.PostAsync("Http function url",content);
            return response;
        }
    }
}
