﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace TC.EMEA.PropertyEtl.ClientApp.Models
{
    public class S1PropertyModel
    {
        [DisplayName("Id")]
        public int Id { get; set; }

        [DisplayName("Address1")]
        public string Address1 { get; set; }

        [DisplayName("Address2")]
        public string Address2 { get; set; }

        [DisplayName("Latitude")]
        public double? Lat { get; set; }
        
        [DisplayName("Longitude")]
        public double? Lon { get; set; }

        [DisplayName("Floor Count")]
        public int? FloorCount { get; set; }
    }
}
