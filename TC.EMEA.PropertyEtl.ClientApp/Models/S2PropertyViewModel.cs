﻿using System.ComponentModel;
using Microsoft.AspNetCore.Http;

namespace TC.EMEA.PropertyEtl.ClientApp.Models
{
    public class S2PropertyViewModel
    {
        [DisplayName("Select file")]
        public IFormFile S2PropertyFile { set; get; }
    }
}