﻿using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using TC.EMEA.PropertyEtl.Business.Orchestrators;
using TC.EMEA.PropertyEtl.Common.Data;
using TC.EMEA.PropertyEtl.Common.Enums;
using TC.EMEA.PropertyEtl.Common.Mapping.Profiles;
using TC.EMEA.PropertyEtl.Common.Models;
using TC.EMEA.PropertyEtl.Common.Orchestrators;
using TC.EMEA.PropertyEtl.Common.Requests;
using TC.EMEA.PropertyEtl.Common.Responses;
using TC.EMEA.PropertyEtl.Common.Specifications;
using TC.EMEA.PropertyEtl.Data.Contexts;
using TC.EMEA.PropertyEtl.Data.Helpers;
using TC.EMEA.PropertyEtl.Data.Repositories;

namespace TC.EMEA.PropertyEtl.Console
{
    class Program
    {
        static async Task Main(string[] args)
        {
            System.Console.WriteLine("Initialising...");
            ConfigureServices();
            //var service = Container.GetRequiredService<IRepository<Property>>();
            //RepositoryMethods(service);
            //await PopulateDataBase();
        }

        private static async Task PopulateDataBase()
        {
            var repositoryHelper = Container.GetRequiredService<RepositoryHelper>();
            await repositoryHelper.EnsureCreated();
            await repositoryHelper.CreateProperties();
        }

        public static void ConfigureServices()
        {
            var configurationBuilder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appSettings.json");
            IConfigurationRoot config = configurationBuilder.Build();
            IConfigurationSection configSection = config.GetSection("CosmosSettings");
            var services = new ServiceCollection();
            services.AddDbContext<DbContext,PropertiesContext>(options => options.UseCosmos(configSection["Endpoint"], configSection["AuthKey"], configSection["DataBaseName"]));
            services.AddTransient(typeof(IRepository<>), typeof(Repository<>));
            services.AddTransient<RepositoryHelper>();
            //services.AddTransient<IOrchestrator<PropertyOrchestrationRequest, PropertyOrchestrationResponse>, PropertyDataOrchestrator>();
            services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());

            var mappingConfig = new MapperConfiguration(cfg => {
                cfg.AddProfile<PropertyProfile>();
            });
            var mapper = mappingConfig.CreateMapper();
            services.AddSingleton(mapper);

            Container = services.BuildServiceProvider();
        }

        public static ServiceProvider Container { get; private set; }
    }
}
