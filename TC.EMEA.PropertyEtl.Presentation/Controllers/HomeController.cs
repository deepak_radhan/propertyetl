﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using TC.EMEA.PropertyEtl.Common.Data;
using TC.EMEA.PropertyEtl.Common.Models;
using TC.EMEA.PropertyEtl.Data.Repositories;
using TC.EMEA.PropertyEtl.Presentation.Models;

namespace TC.EMEA.PropertyEtl.Presentation.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IRepository<Property> _propertyRepository;

        public HomeController(ILogger<HomeController> logger, IRepository<Property> propertyRepository)
        {
            _logger = logger;
            _propertyRepository = propertyRepository;
        }

        public IActionResult Index()
        {
            var propertyList = this._propertyRepository.Get(x => true)
                                                                                .OrderBy(x => x.SourceSystem)
                                                                                .ThenBy(x => x.SourcePropertyId);
            return View(propertyList);
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
